const HTTP = require('http');
const PORT = 4000;


HTTP.createServer( (req, res) => {   

    if (req.url == '/') { 
             
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        res.write('Welcome to booking system');
        res.end();
    
    }
    else if (req.url == "/profile") {
        
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        res.write('Welcome to your profile');
        res.end();
    
    }
    else if (req.url == "/courses") {
        
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        res.write(`Here's our courses available`);
        res.end();
    }
    else if (req.url == "/addCourses") {
        
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        res.write(`Add course to our resources`);
        res.end();
    }
    else if (req.url == "/updateCourse") {
        
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        res.write(`Update a course to our resources`);
        res.end();
    }
    else if (req.url == "/archiveCourse") {
        
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        res.write(`Archive course to our resources`);
        res.end();
    }
    else
        res.end('Page not Found');

}).listen(PORT, () => console.log(`Connected to port 4000`));